resource "aws_secretsmanager_secret" "rds-proxy-test" {
  kms_key_id  = module.kms-cmk.id
  name_prefix = "rds-proxy-test"
}

locals {
  rds-proxy-test-secret = {
    username            = aws_rds_cluster.rds-proxy-test.master_username
    password            = random_password.password.result
    engine              = "postgres"
    host                = aws_rds_cluster.rds-proxy-test.endpoint
    port                = "5432"
    dbname              = aws_rds_cluster.rds-proxy-test.database_name
    dbClusterIdentifier = aws_rds_cluster.rds-proxy-test.cluster_identifier
  }
}

resource "aws_secretsmanager_secret_version" "rds-proxy-test" {
  secret_id     = aws_secretsmanager_secret.rds-proxy-test.id
  secret_string = jsonencode(local.rds-proxy-test-secret)
}
