#!/bin/bash

if [ ! "$RDSHOST" ]; then
  printf "Please export the RDS proxy endpoint as the RDSHOST env variable!\n"
  exit 1
fi

if [ ! -e AmazonRootCA1.pem ]; then
  wget https://www.amazontrust.com/repository/AmazonRootCA1.pem
fi

export PGPASSWORD="$(aws rds generate-db-auth-token --hostname $RDSHOST --port 5432 --region us-east-1 --username postgres)"

psql "host=$RDSHOST port=5432 sslmode=verify-full sslrootcert=AmazonRootCA1.pem dbname=mydb user=postgres password=$PGPASSWORD"
