#!/bin/bash

if [ ! -e rds-proxy-test.pem ]; then
  ssh-keygen -q -t rsa -N '' -C "rds-proxy-test" -f rds-proxy-test.pem
fi

PUBLIC_KEY="`cat rds-proxy-test.pem.pub`"
jq -n --arg public_key "$PUBLIC_KEY" '{"public_key":$public_key}'
