import sys
import boto3
import psycopg2
import os

ENDPOINT=os.getenv("RDSHOST")
if ENDPOINT == None:
  print("Please export the RDS proxy endpoint as the RDSHOST env variable!")
  sys.exit()
PORT="5432"
USR="postgres"
REGION="us-east-1"
DBNAME="mydb"

client = boto3.client('rds', region_name=REGION)

token = client.generate_db_auth_token(DBHostname=ENDPOINT, Port=PORT, DBUsername=USR, Region=REGION)

try:
  conn = psycopg2.connect(host=ENDPOINT, port=PORT, database=DBNAME, user=USR, password=token)
  cur = conn.cursor()
  cur.execute("""SELECT now()""")
  query_results = cur.fetchall()
  print(query_results)
except Exception as e:
  print("Database connection failed due to {}".format(e))
