data "aws_ami" "amazon-linux-2" {
 most_recent = true
 filter {
   name   = "name"
   values = ["amzn2-ami-hvm*"]
 }
 owners      = ["amazon"]
}

data "external" "ssh-keygen" {
  program = ["bash", "${path.module}/ssh-keygen.sh"]
}

resource "aws_key_pair" "rds-proxy-test" {
  key_name   = "rds-proxy-test"
  public_key = data.external.ssh-keygen.result["public_key"]
}

data "template_file" "user-data" {
  template = file("${path.module}/templates/user-data.tpl")
}

resource "aws_instance" "rds-proxy-test" {
  ami                    = data.aws_ami.amazon-linux-2.id
  iam_instance_profile   = aws_iam_instance_profile.rds-proxy-test-ec2-role.name
  instance_type          = "t3a.micro"
  key_name               = aws_key_pair.rds-proxy-test.id
  tags = {
    Name = "rds-proxy-test"
  }
  user_data              = base64encode(data.template_file.user-data.rendered)
  vpc_security_group_ids = [aws_security_group.rds-proxy-test-ec2.id]
}

