resource "random_password" "password" {
  length = 16
  special = false
}

resource "aws_rds_cluster" "rds-proxy-test" {
  availability_zones           = [for s in data.aws_subnet.default : s.availability_zone]
  cluster_identifier           = "rds-proxy-test"
  database_name                = "mydb"
  db_subnet_group_name         = aws_db_subnet_group.rds-proxy-test.id
  engine                       = "aurora-postgresql"
  master_password              = random_password.password.result
  master_username              = "postgres"
  skip_final_snapshot          = true
  tags = {
    Name = "rds-proxy-test"
  }
  vpc_security_group_ids       = [aws_security_group.rds-proxy-test-aurora.id]
}

resource "aws_rds_cluster_instance" "rds-proxy-test" {
  cluster_identifier           = aws_rds_cluster.rds-proxy-test.id
  db_subnet_group_name         = aws_db_subnet_group.rds-proxy-test.id
  engine                       = "aurora-postgresql"
  identifier                   = "rds-proxy-test-a"
  instance_class               = "db.t3.medium"
  publicly_accessible          = "true"
  tags = {
    Name = "rds-proxy-test-a"
  }
}
