resource "aws_db_subnet_group" "rds-proxy-test" {
  subnet_ids  = data.aws_subnet_ids.default.ids
}
