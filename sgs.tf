resource "aws_security_group" "rds-proxy-test-aurora" {
  description = "rds-proxy-test-aurora"
  name        = "rds-proxy-test-aurora"
  tags = {
    Name = "rds-proxy-test-aurora"
  }
  vpc_id = data.aws_vpc.default.id
}

resource "aws_security_group_rule" "rds-proxy-test-aurora-inbound" {
  description              = "PostgreSQL (5432) access from the RDS proxy."
  from_port                = "5432"
  protocol                 = "tcp"
  security_group_id        = aws_security_group.rds-proxy-test-aurora.id
  source_security_group_id = aws_security_group.rds-proxy-test-proxy.id
  to_port                  = "5432"
  type                     = "ingress"
}

resource "aws_security_group" "rds-proxy-test-proxy" {
  description = "rds-proxy-test-proxy"
  name        = "rds-proxy-test-proxy"
  tags = {
    Name = "rds-proxy-test-proxy"
  }
  vpc_id = data.aws_vpc.default.id
}

resource "aws_security_group_rule" "rds-proxy-test-proxy-inbound" {
  description              = "PostgreSQL (5432) access from the EC2 instance."
  from_port                = "5432"
  protocol                 = "tcp"
  security_group_id        = aws_security_group.rds-proxy-test-proxy.id
  source_security_group_id = aws_security_group.rds-proxy-test-ec2.id
  to_port                  = "5432"
  type                     = "ingress"
}

resource "aws_security_group_rule" "rds-proxy-test-proxy-outbound" {
  description              = "PostgreSQL (5432) access to the RDS Aurora cluster"
  from_port                = "5432"
  protocol                 = "tcp"
  security_group_id        = aws_security_group.rds-proxy-test-proxy.id
  source_security_group_id = aws_security_group.rds-proxy-test-aurora.id
  to_port                  = "5432"
  type                     = "egress"
}

resource "aws_security_group" "rds-proxy-test-ec2" {
  description = "rds-proxy-test-ec2"
  name        = "rds-proxy-test-ec2"
  tags = {
    Name = "rds-proxy-test-ec2"
  }
  vpc_id = data.aws_vpc.default.id
}

data "external" "ifconfig" {
  program = ["bash", "${path.module}/ifconfig.sh"]
}

resource "aws_security_group_rule" "rds-proxy-test-ec2-inbound" {
  cidr_blocks              = [data.external.ifconfig.result["public_ip"]]
  description              = "Allowing SSH access"
  from_port                = "22"
  protocol                 = "tcp"
  security_group_id        = aws_security_group.rds-proxy-test-ec2.id
  to_port                  = "22"
  type                     = "ingress"
}

resource "aws_security_group_rule" "rds-proxy-test-ec2-outbound" {
  description              = "PostgreSQL (5432) access to the RDS proxy"
  from_port                = "5432"
  protocol                 = "tcp"
  security_group_id        = aws_security_group.rds-proxy-test-ec2.id
  source_security_group_id = aws_security_group.rds-proxy-test-proxy.id
  to_port                  = "5432"
  type                     = "egress"
}

resource "aws_security_group_rule" "rds-proxy-test-ec2-http-outbound" {
  cidr_blocks              = ["0.0.0.0/0"]
  description              = "Allowing HTTP outbound access"
  from_port                = "80"
  protocol                 = "tcp"
  security_group_id        = aws_security_group.rds-proxy-test-ec2.id
  to_port                  = "80"
  type                     = "egress"
}

resource "aws_security_group_rule" "rds-proxy-test-ec2-https-outbound" {
  cidr_blocks              = ["0.0.0.0/0"]
  description              = "Allowing HTTPS outbound access"
  from_port                = "443"
  protocol                 = "tcp"
  security_group_id        = aws_security_group.rds-proxy-test-ec2.id
  to_port                  = "443"
  type                     = "egress"
}

