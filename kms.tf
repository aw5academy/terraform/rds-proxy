resource "random_string" "kms-suffix" {
  length = 8
  special = false
}

module "kms-cmk" {
  cmk-name = "rds-proxy-test-${random_string.kms-suffix.result}"
  source   = "git::https://gitlab.com/aw5academy/terraform/modules/kms-cmk.git"
  region   = var.region
}
