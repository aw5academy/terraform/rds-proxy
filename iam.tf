data "aws_iam_policy_document" "rds" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["rds.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "rds-proxy-test-proxy-role" {
  assume_role_policy = data.aws_iam_policy_document.rds.json
  name               = "rds-proxy-test-proxy-role"
  tags = {
    Name = "rds-proxy-test-proxy-role"
  }
}

data "template_file" "rds-proxy-test-proxy-role" {
  template = file("${path.module}/templates/rds-proxy-test-proxy-policy.json.tpl")
  vars = {
    kms-key-arn = module.kms-cmk.arn
    region      = var.region
    secret-arn  = aws_secretsmanager_secret.rds-proxy-test.arn
  }
}

resource "aws_iam_role_policy" "rds-proxy-test-proxy-role" {
  name   = "rds-proxy-test-proxy-role"
  policy = data.template_file.rds-proxy-test-proxy-role.rendered
  role   = aws_iam_role.rds-proxy-test-proxy-role.id
}

data "aws_iam_policy_document" "ec2" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "rds-proxy-test-ec2-role" {
  assume_role_policy = data.aws_iam_policy_document.ec2.json
  name               = "rds-proxy-test-ec2-role"
  tags = {
    Name = "rds-proxy-test-ec2-role"
  }
}

resource "aws_iam_instance_profile" "rds-proxy-test-ec2-role" {
  name  = "rds-proxy-test-ec2-role"
  role  = aws_iam_role.rds-proxy-test-ec2-role.name
}

data "template_file" "rds-proxy-test-ec2-role" {
  template = file("${path.module}/templates/rds-proxy-test-ec2-policy.json.tpl")
}

resource "aws_iam_role_policy" "rds-proxy-test-ec2-role" {
  name   = "rds-proxy-test-ec2-role"
  policy = data.template_file.rds-proxy-test-ec2-role.rendered
  role   = aws_iam_role.rds-proxy-test-ec2-role.id
}
