# rds-proxy
Creates an [Amazon Aurora](https://aws.amazon.com/rds/aurora/) RDS cluster that can be used with an [Amazon RDS Proxy](https://aws.amazon.com/rds/proxy/).

# Usage
* Deploy the stack with:
  ```
  git clone https://gitlab.com/aw5academy/terraform/rds-proxy.git
  cd rds-proxy
  terraform init
  terraform apply
  ```
* Create an RDS proxy in the AWS console providing the following details:
  1. Select `PostgreSQL` for Enginge compatibility;
  2. Tick `Require Transport Layer Security`;
  3. Select `rds-proxy-test` for Database;
  4. Select the secret with prefix `rds-proxy-test` for Secrets Manager secret(s);
  5. Select `rds-proxy-test-proxy-role` for IAM role;
  6. Select `Required` for IAM authentication;
  7. Select `rds-proxy-test-proxy` for Existing VPC security groups;
* Wait for the RDS proxy to become available. This may take some time.
* Obtain the RDS proxy endpoint for the proxy from the RDS console.
* SSH into the EC2 instance with:
  ```
  ssh -i rds-proxy-test.pem ec2-user@`terraform output ec2-public-ip`
  ```
* Export the RDS proxy endpoint as `RDSHOST`. E.g.
  ```
  export RDSHOST=rds-proxy-test.proxy-abcdefghijkl.us-east-1.rds.amazonaws.com
  ```
* Test connecting to the proxy with psql:
  ```
  ./proxy.sh
  ```
* Test connecting to the proxy with python:
  ```
  python3.8 proxy.py
  ```

# Notes
In the `templates/rds-proxy-test-ec2-policy.json.tpl` we have a policy allowing `rds-db:connect` access for all resources. In practice, this should be limited to the ARN of the RDS proxy that you create.

# Cleanup
Cleanup this stack by deleting the RDS proxy from the AWS console and then running the following commands:
```
terraform init
terraform destroy
```
