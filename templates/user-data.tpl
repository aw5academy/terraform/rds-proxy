#!/bin/bash

amazon-linux-extras install -y postgresql11
amazon-linux-extras install -y python3.8
yum install -y postgresql postgresql-devel python-devel gcc
pip3.8 install boto3
pip3.8 install psycopg2-binary
wget "https://gitlab.com/aw5academy/terraform/rds-proxy/-/raw/master/proxy.sh" -O /home/ec2-user/proxy.sh
wget "https://gitlab.com/aw5academy/terraform/rds-proxy/-/raw/master/proxy.py" -O /home/ec2-user/proxy.py
chown ec2-user: /home/ec2-user/proxy.sh /home/ec2-user/proxy.py
chmod 755 /home/ec2-user/proxy.sh /home/ec2-user/proxy.py
